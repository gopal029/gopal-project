<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>OTP Generator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
    

        function randomNumber(min, max) {
          return Math.ceil(Math.random() * (max - min) + min);
        }
        var otp = '';
      $(document).ready(function(){
        $('.otp').hide();
        $('.check').hide();
        $('.generate').click(function(){
          otp = randomNumber(1000,9999);
          var number = $('.number').val();
          $('.number').attr('disabled','disabled');
          $('.otp').show();
          $('.check').show();
          $('.generate').hide();
          //alert(otp);
          $.ajax({
            url:'http://localhost/b111/d903/sendotp.php',
            type: 'post',
            data: {otp:otp,number:number},
            success: function(data){
              alert("OTP SEND");
            }
          })
        });
        $('.check').click(function(){
          var user_otp = $('.otp').val();
          if(otp==user_otp){
            alert('Succcess');
          }else{
            alert('Error');
          }
        });
      });
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 bg-success p-5 mx-auto mt-5">
          <h1 class="text-center">OTP Generator</h1>
          <hr/>
          <label>Enter Mobile No</label>
          <input type="number" class="form-control number" />
          <input type="number" class="form-control mt-3 otp" placeholder="ENTER OTP"/>
          <input type="button" class="btn btn-dark mt-3 generate" value="Send OTP"/>
          <input type="button" class="btn btn-dark mt-3 check" value="Check"/>
        </div>
      </div>
    </div>
  </body>
</html>