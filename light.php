<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>LIGHTS</title>
    <style>
      .box{width: 150px; height: 150px; margin:20px 0px 20px 0px;}
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="abcd.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12 bg-dark text-light">
          <h1 id='count' class="text-center my-5"> Counter Begin Here</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 border border-dark px-0">
          <h1 class="box1 bg-dark text-center text-white py-3">0</h1>
          <div class="box rounded-circle bg-danger mx-auto" id='b1-l1'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b1-l2'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b1-l3'></div>
        </div>
        <div class="col-md-3 border border-dark px-0">
          <h1 class="box2 bg-dark text-center text-white py-3">0</h1>
          <div class="box rounded-circle bg-danger mx-auto" id='b2-l1'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b2-l2'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b2-l3'></div>
        </div>
        <div class="col-md-3  border border-dark px-0">
          <h1 class="box3 bg-dark text-center text-white py-3">0</h1>
          <div class="box rounded-circle bg-danger mx-auto" id='b3-l1'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b3-l2'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b3-l3'></div>
        </div>
        <div class="col-md-3 border border-dark px-0">
          <h1 class="box4 bg-dark text-center text-white py-3">0</h1>
          <div class="box rounded-circle bg-danger mx-auto" id='b4-l1'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b4-l2'></div>
          <div class="box rounded-circle bg-dark mx-auto" id='b4-l3'></div>
        </div>
      </div>
    </div>
  </body>
  </html>