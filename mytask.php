<?php
session_start();
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
    <script src='js/bootstrap.js'></script>
    <script>
      $(document).ready(function(){
        $('#myform').submit(function(event){
          event.preventDefault();
          var user = $('#user').val();
          var pass = $('#pass').val();
          $.ajax({
            url:'alogin.php',
            type: 'post',
            data: {username:user,password:pass},
            success: function(data){
              if(data == 'success'){
                $('.msg').html("<div class='alert alert-success'>Success</div>");
                window.location = 'adashboard.php';
              }else{
                $('.msg').html("<div class='alert alert-danger'>OOPS</div>");
              }
            }
          });
        });
      });
    </script>
    <title></title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="modal" id="login" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <h1 class="text-center">Login</h1>
                  <hr/>
                  <div class="msg"></div>
                  <form id="myform">
                    <label>Username</label>
                    <input type="text" class="form-control" id='user' />
                    <label>Password</label>
                    <input type="password" class="form-control" id='pass' />
                    <input type="submit" class="btn btn-success btn-md mt-3" value="Login" />
                    <input type="button" class="btn btn-danger btn-md mt-3" value="Close"data-dismiss="modal" />
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php
            if(isset($_SESSION['login'])){
              if($_SESSION['login']===true){
                echo "<a href='logout.php' class='btn btn-danger'>Log Out</a>";
              }else{
                echo "<button class='btn btn-success' data-toggle='modal' data-target='#login'>Login</button>";
              }
            }else{
              echo "<button class='btn btn-success' data-toggle='modal' data-target='#login'>Login</button>";
            }
          ?>


        </div>
      </div>
    </div>
  </body>
</html>