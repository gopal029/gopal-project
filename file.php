<!DOCTYPE html>
<html>
	<head>
		<title>file upload</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	<script src="jsquery.js"></script>

    <script>
    $(document).ready(function(){
        $('#myform').submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'upload.php',
                type:'POST',
                data: new FormData(this),
                contentType:false,
                cache: false,
                processData: false,
                success: function(data){
                    alert(data);
                }
            });
        });
    });
    </script>
	</head>
    <body>
    <form action="" id="myform" method="post" enctype="multipart/form-data">
    <input type="file" name="myfile"/>
    <input type="text" name="myname"/>
    <input type="submit"/>
</form>
	
	</body>
	</html>
