<?php
  include_once('connection.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign | Up</title>
    <link rel="stylesheet" href="css/bootstrap.css">
  </head>
  <body class="bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-6 bg-light mx-auto m-5 p-5">
          <form action="update.php" method="post" >
          <?php
            if(isset($_GET['id']))
            {
              $id = $_GET['id'];
              $qry = "SELECT * FROM signup WHERE id='$id'";
              $result = $connect->query($qry);
              $arr = $result->fetch_assoc();

            }
            ?>
          <h1 class="text-center">Now You Can Edit Your Details.</h1>
          <hr>
            <div class="row">
              <div class="col-md-6 form-group">
                <label>First Name</label>
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <input type="text" name="first_name" value="<?php echo $arr['first_name']; ?>" class="form-control" required />
              </div>
              <div class="col-md-6 form-group">
                <label>Last Name</label>
                <input type="text" name="last_name" value="<?php echo $arr['last_name']; ?>" class="form-control" required />
              </div>
            </div>
            <label>E-mail</label>
            <input type="email" name="email" value="<?php echo $arr['email']; ?>" class="form-control" required />
            <div class="form-group form-check mt-3">
              <label class="form-check-label">
               <input type="checkbox"  class="form-check-input"/>I accept all terms and condition.
            </label>
            </div>
            <input type="submit" class="btn btn-success mt-3 btn-block" value="Register" />
          </form>
        </div>
      </div>
    </div>
  </body>
</html>