<html>
<head>
<title></title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="jsquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
    $(document).ready(function(){
        $('#myform').submit(function(e){
            e.preventDefault();
            var user = $('#user').val();
            var pass = $('#pass').val();
          $.ajax({
              url : 'login.php',
              type : 'post',
              data : (username:user,password:pass),
              success : function(data){
                  if(data=='success'){
                      $('.msg').html("<div class='alert alert-success'>Success</div>");
                      window.location = "dashboard5.php";

                  }else
                  {
                    $('.msg').html("<div class='alert alert-danger'>OOPS</div>");

                  }
              }
          });
        });
    });
    </script>
</head>
<body>
<div class="modal" id="mymodal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<h1>Login</h1>
<hr/>
<div class="msg"></div>

<form id="myform">
<label>Username</label>
<input type="text" class="form-control" id="user"/>
<label>Password</label>
<input type="text" class="form-control" id="pass"/>
<input type="submit" class="btn btn-success mt-3" value="Login" />
<input type="button" class="btn btn-danger mt-3" value="Close" />
</form>
</div>
</div>
</div>
</div>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#mymodal">login</button>

</body>
</html>
