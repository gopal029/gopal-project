<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Task</title>
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <style>
    body {font-family: "Lato", sans-serif;}

.sidebar {
  height: 100%;
  width: 160px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background: #0f1b35;
  overflow-x: hidden;
  padding-top: 16px;
}

.sidebar a {
  padding: 6px 8px 6px 16px;
  text-decoration: light;
  font-size: 20px;
  color: dark;
  display: block;
}




    input{
        height:30px;
        border-radius: 21% !important;
    }
    img{
        border-radius: 60% !important;
    }
    </style>
    <body class="bg-light">
    <div class="container">
    <div class="row mt-3">
    <div class="col-md-1 pl-5">
    <i class="fas fa-align-justify"></i>
    <div class="sidebar">
    <a href="#Mercury">  <i class="fas fa-dot-circle"></i> Mercury</a>
  <a href="#Home"><i class="fa fa-fw fa-home"></i> Home</a>
  <a href="#Workflow">    <i class="fas fa-align-justify"></i> Workflow</a>
  <a href="#Statics"><i class="fab fa-creative-commons-sampling"></i> Statics</a>
  <a href="#Calender"><i class="fas fa-calendar-alt"></i> Calender</a>
  <a href="#Users"><i class="fa fa-fw fa-users"></i> Users</a>
  <a href="#Settings"><i class="fa fa-fw fa-wrench"></i> Settings</a>


</div>
    </div>
    <div class="col-md-4">
    <i class="fas fa-search"></i>
    </div>
    <div class="col-md-4  text-right">
     <input type="submit" class="btn btn-primary pl-2" style="font-size:12px" value="+ Add Project"/>

    <i class="far fa-envelope pr-3"></i>
    
    <i class="fas fa-bell pr-3"></i>
    
    <img src="photo.jpg"/>
    </div>
  </body>
  </html>