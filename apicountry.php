<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<title>Countries | API</title>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
<script>

$(document).ready(function(){
$.getJSON('https://restcountries.eu/rest/v2/all', function(data){
var createoption = "<select id='country' class='form-control my-3' onchange='getDetails()'>";
for(d in data){
createoption += "<option>" + data[d].name + "</option>";
}
createoption += "</select>";
document.getElementById('result').innerHTML = createoption;
});
});
function getDetails(){
var a = $('#country').val();
var url = "https://restcountries.eu/rest/v2/name/"+a+"?fullText=true";
$.getJSON(url, function(data){
$('#flag').html('<img src="'+data[0].flag+'" style="width:200px;"/>')

});
}
</script>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-6 mx-left">
<div id="result"></div>
<div class="countrydetails">
<table class="table table-light">
<tbody>
<tr>
<th>Flag</th>
<td id='flag'></td>
</tr>

</tbody>
</table>
</div>
</div>



<form action="process.php" method="POST">
<div class="col-md-6">
<div class="form.group">
<p class="text-center">Create your account <p>
<label>First Name</label>
<input type="text" class="form-control" />

<label>Phone Number</label>
<input type="input" class="form-control">
<input type="checkbox" class="mt-3">I agree with term and condition.</br>
<input type"BUTTON" class="btn btn-success btn-block mt-3" value="Register" />
</form>
</div>
</div>


</body>
</html>