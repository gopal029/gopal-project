<?php
	session_start();
	include_once('cookie.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link href="css/bootstrap.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" />
		<style>
			body{
				background-color : midnightblue;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row mt-5 py-5">
				<div class="col-md-12 ">
					<div class="row">
						<div class="col-md-6 bg-info text-light pb-5 mt-5">
							<form method="post" action="dbase1.php" >
							<h4 class="mx-5 display-4 text-light mt-5">Static Login Form</h4>
							<p class="mx-5 mt-4 text-light"><i class="far fa-dot-circle"></i>&nbsp;&nbsp;&nbsp;It has roots in a piece</p>
							<p class="mx-5 text-light"><i class="far fa-circle"></i>&nbsp;&nbsp;&nbsp;Sed ut perspiciatis under</p>
							<p class="mx-5 text-light text-monospace"><i class="far fa-circle"></i>&nbsp;&nbsp;&nbsp;Duis aute irure dolor re</p>
						</div>
						<div class="col-md-6 bg-light pt-3 mt-5">
							<a href="">Login</a><a href="singup.php">/ Sign up</a>
							<?php
								if(isset($_GET['status']))
								{
									echo "<div class='alert alert-success text-center'>".$_GET['status']."</div>";
								}
								?>
								<?php
									if(isset($_GET['msg']))
									{
										echo "<div class='alert alert-success text-center'>".$_GET['msg']."</div>";
									}
									?>
									<?php
										if(isset($_GET['b']))
										{
											echo "<div class='alert alert-success text-center'>".$_GET['b']."</div>";
										}
										?>
						<div class="row border-bottom">

						   <div class="col-md-10 mt-3">

							   <input type="email" name="email" placeholder="Email" class="form-control"></div>

								<div class="col-md-2 mt-3"><i class="fas fa-envelope"></i></div>
						</div>
						<div class="row border-bottom">

								<div class="col-md-10 mt-3"> <input type="password" placeholder="Password" name="password" class="form-control"></div>

								<div class="col-md-2 mt-3"><i class="fas fa-lock"></i></div>
						</div>
								<div class="mt-5">
								<input type="submit" class="btn btn-outline-info  ml-5" style="border-radius:30px;" value="LOGIN"/>
								<span style="color:grey; float:right;"> Or login with </style> <i style="color:midnightblue;" class="fab fa-facebook"></i> <i style="color:skyblue;" class="fab fa-twitter-square"></i> <i style="color:red;" class="fab fa-google-plus"></i>
							</div>
						</form>
						</div>
						</div>
						<div class="col-md-12 mt-3 text-center">
								<span style="color:grey">Don't have an</span> <span style="color:white">account?&nbsp;&nbsp;&nbsp;/</span>
								<span style="color:grey">Forgot</span> <span style="color:white">password?</span>
						 </div>
					</div>
				</div>
			</div>
	</body>
</html>