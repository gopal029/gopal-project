<?php
  include_once('session.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit | Data</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <thead>
              <th>ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php
                include_once('connection.php');
                $qry = "SELECT * FROM signup";
                $result = $connect->query($qry);
                while($row = $result->fetch_assoc())
                {
                  $count = 1;
                  echo "<tr>";
                  echo "<td>$count</td>";
                  echo "<td>".$row['first_name']."</td>";
                  echo "<td>".$row['last_name']."</td>";
                  echo "<td>".$row['email']."</td>";
                  echo "<td> <a href='edit.php?id=".$row['id']."' class='btn btn-warning' >Edit</a></td>";
                  // echo "<td> <a href='editpage.php?id=".$row['id']."' class='btn btn-warning' >Edit</a></td>";
                  echo "<td> <a href='delete.php?id=".$row['id']."' class='btn btn-danger' >Delete</a></td>";
                  echo "</tr>";
                  $count++;

                }
              ?>
            </tbody>
          </table>
          <a href="sessiond.php" class="btn btn-success">Log Out</a>
        </div>
      </div>
    </div>
  </body>
</html>