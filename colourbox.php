

<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
   <meta charset="utf-8">
   <title></title>
   <style media="screen">
     .box
     {
       height:100px;
       width:100px;
       background: white;
     }
   </style>
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <script>
     $(document).ready(function(){
       $('#color').keyup(function(){
         var a = $('#color').val();
         $(".box").css("background",""+a+"");
       });
     });
   </script>
 </head>
 <body>
   <input type="text" id="color" placeholder="Please Enter Color Name" />
   <div class="box">

   </div>
 </body>
</html>