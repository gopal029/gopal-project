<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tax Calculator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	<script src="jsquery.js"></script>
    <script>
      function calculatetax(){
        var a = document.getElementById('amount').value;
        var b = document.getElementById('tax').value;
        var c = a * b / 100;
        var d = a - c;
        a = parseInt(a);
        var total = a + c;
        var dd = "<ul class='list-group'><li class='list-group-item'><strong>Total amount : "+total+"</strong></li><li class='list-group-item'>Tax : "+c+"</li><li class='list-group-item'>Base Amount: "+a+"</li></ul>";
        document.getElementById('result').innerHTML = dd;
      }
    </script>
  </head>
  <body class="bg-dark">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 bg-light mt-5 mx-auto p-5">
          <h1 class="text-center">Tax Calculator</h1>
          <hr/>
          <div class='form-group'>
            <label>Total Amount</label>
            <input type="number" class="form-control" id='amount' />
            <label>Tax %</label>
            <input type="number" class="form-control" id='tax' />
            <input type="button" class="btn btn-success mt-3" onclick="calculatetax();" value="Calculate" />
            <div class='mt-3' id='result'></div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>