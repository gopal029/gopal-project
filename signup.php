<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign | Up</title>
    <link rel="stylesheet" href="css/bootstrap.css">
  </head>
  <body class="bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-6 bg-light mx-auto m-5 p-5">
          <form action="dbase.php" method="post" >
          <?php
            if(isset($_GET['d']))
            {
              echo "<div class='alert alert-success text-center'>".$_GET['d']."</div>";
            }
            ?>
            <?php
              if(isset($_GET['c']))
              {
                echo "<div class='alert alert-success text-center'>".$_GET['c']."</div>";
              }
              ?>
          <a href="signin.php" class="alert alert-light bg-light">Back to Login Page</a>
          <h1 class="text-center">Register</h1>
          <p class="text-center">Create your account.It's free and only takes a minute.</p>

            <div class="row">
              <div class="col-md-6 form-group">
                <label>First Name</label>
                <input type="text" name="first_name" class="form-control" required />
              </div>
              <div class="col-md-6 form-group">
                <label>Last Name</label>
                <input type="text" name="last_name" class="form-control" required />
              </div>
            </div>
            <label>E-mail</label>
            <input type="email" name="email" class="form-control" required />
            <label>Password</label>
            <input type="password" name="password" class="form-control" required />
            <label>Confirm Password</label>
            <input type="password" name="confirm_password" class="form-control" required />
            <div class="form-group form-check mt-3">
              <label class="form-check-label">
               <input type="checkbox"  class="form-check-input"/>I accept all terms and condition.
            </label>
            </div>
            <input type="submit" class="btn btn-success mt-3 btn-block" value="Register" />
          </form>
        </div>
      </div>
    </div>
  </body>
</html>