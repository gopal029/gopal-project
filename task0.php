<?php
class form{
  function __construct(){
    echo "<form>";
  }
  public function input_field($data = array()){
    $attribute = '';
    if(count($data) > 0){
      foreach ($data as $key => $value) {
        $attribute .= "$key='$value'";
      }
    }
    return "<input type='text' $attribute />";
  }
  function close_form(){
    echo "</form>";
  }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title></title>
  </head>
  <body class="bg-dark">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 mx-auto bg-light p-5 mt-5">
          <h1>My Form</h1>
          <hr/>
          <?php
          $obj = new form();
          echo $obj->input_field(array('placeholder' => 'Full Name', 'class' => 'form-control'));
          //echo $obj->getyearsfield(2012,2018,array('class'=>'form-control'));
          echo $obj->close_form();
          ?>
        </div>
      </div>
    </div>
  </body>
</html>