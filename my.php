<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src='jsquery.js'></script>
    <script>
      $(document).ready(function(){

        $('#myform').submit(function(e){
          e.preventDefault();
          var a = $('#username').val();
          var b = $('#password').val();
          var c = $('#fullname').val();

          $('#username').val('');
          $('#password').val('');
          $('#fullname').val('');
          $('#username').focus();
          $.ajax({
            url: 'registerpage.php',
            type: 'post',
            data: {username:a,password:b,fullname:c},
            success: function(data){
              $('.showdata').html(data);
            }
          });
        });
      });
    </script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1>Register</h1>
          <form id='myform'>
            <label>Username</label>
            <input type="text" id='username' class="form-control"/>
            <label>Password</label>
            <input type="password" id='password' class="form-control"/>
            <label>Full Name</label>
            <input type="text" id='fullname' class="form-control"/>
            <input type="submit" class="btn btn-success mt-3" value="Register" />
          </form>
        </div>
        <div class="col-md-6 showdata">
          <?php include_once('registerpage.php'); ?>
        </div>
      </div>
    </div>
  </body>
</html>