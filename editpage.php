<?php
include_once('connect.php')
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Form 2</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"  crossorigin="anonymous">
    <style>
      .a
      {
        border-radius: 0px;
      }
      .box
      {
        margin-top: -80px;
        width: 80px;
        height: 80px;
        border-radius: 50%;
        border: 0.5px solid grey;
        background: white;
        text-align: center;
        line-height: 80px;
        font-size: 40px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-6 mx-auto m-5 p-5 text-right my-5" style="background: white;">
          <div class="box mx-auto">
            <i class="far fa-envelope"></i>
          </div>
          
          <h1 class="text-center">Drop Us A Message</h1>
		  <?php
		  if(isset($_GET['status'])){
			  echo "<div class='alert alert-success'>".$_GET['status']."</div>";
		  }
		  if(isset($_GET['id'])){
		   $id = $_GET['id'];
		   $qry = "SELECT * FROM contact WHERE id='$id'";
		   $result = $connect->query($qry);
		   $arr = $result->fetch_assoc();
		   echo "<pre>";
		   print_r($arr);
		   echo "</pre>";
		   }
		  ?>
		  <form action="action.php" method="post">
		  <input type="hidden" name="id" value="<?php echo $id; ?>"  />
		  
          <div class="row mt-3">
            <div class="col-md-6 form-group">
              <input type="text" placeholder="Name" name="fullname" class="form-control a" />
            </div>
            <div class="col-md-6 form-group">
              <input type="email" placeholder="Email address" name="email" class="form-control a" />
            </div>
          </div>
          <textarea rows="5" cols="50" type="text" name="message" placeholder="Write Us A Message" class="form-control a"></textarea>
          <input type="submit" value="Send" class="btn btn-danger mt-3  a" />
        </form>
        </div>
      </div>

    </div>
  </body>
</html>