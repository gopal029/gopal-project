<?php
session_start();
  if(isset($_SESSION['login'])){
    if($_SESSION['login']===false){
      echo "Invalid username or password";
      exit();
    }
  }else{
    echo "Session not set";
    exit();
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>DASHBOARD</title>
    <style>
      .counter{
        width: 100px;
        height: 100px;
        background: black;
        border-radius: 50%;
        text-align: center;
        color: white;
        margin:0 auto;
        line-height: 100px;
        font-size:40px;
      }
    </style>
    <script>
      var a = 1;
      function myCount(){
        document.getElementById('counter').innerHTML = a;
        if(a==300){
          window.location = 'logout.php';
        }
        if(a==25){
          document.getElementById('warning').innerHTML = "Your session will destroy soon";
        }
        a++;
      }
      setInterval(myCount,500);
    </script>
  </head>
  <body>
    <h1 align="center">Dashboard</h1>
    <hr/>
    <p id='warning'>Your sesssion will destroy after 30 sec</p>
    <a href="logout.php">Log Out</a>
    <div class="counter" id="counter"></div>
  </body>
</html>