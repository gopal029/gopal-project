<!DOCTYPE html>
<html>
  <head>
    <title>PHP HTML</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" />
    <style media="screen">
      .box{
        width: 80px;
        height: 80px;
        text-align: center;
        line-height: 80px;
        font-size:40px;
        border-radius: 50%;
        margin: 0 auto;
        border:1px solid #ccc;
        margin-top:-80px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12 p-5 mt-5 bg-light">
          <div class="box bg-light">
            <i class="far fa-envelope"></i>
          </div>
          <h1 class="text-center my-5">Drop us a Message</h1>
          <div class="row mt-3">
            <div class="col-md-6">
              <input type="text" name="" class="form-control" placeholder="Name" value="">
            </div>
            <div class="col-md-6">
              <input type="email" name="" class="form-control" placeholder="Email Address" value="">
            </div>
          </div>
          <textarea name="name" class="form-control mt-3" placeholder="write us a message" rows="8" cols="80"></textarea>
          <div class="text-right">
            <button type="button" class="btn btn-danger mt-3" name="button">Send</button>
          </div>

        </div>
      </div>
    </div>
  </body>
</html>